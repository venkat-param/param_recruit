class Client
  include Mongoid::Document
	include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  
  field :bm_id, type: String
  field :name, type: String
  field :website, type: String
  field :address1, type: String
  field :address2, type: String
  field :municipality, type: String
  field :postalcode, type: String 
  field :discountpct, type: String
  field :discountfee, type: String
  field :stockstymbol, type: String   
  field :info, type: String   
  
  validates :name, :bm_id, presence: true
  validates :name, uniqueness: {scope: :bm_id}
  
  def self.import_bmdata_from_xml
    obj1 = Parsexml.new
    total_count, list = obj1.get_files("public", "client")
    STDOUT.puts "Processing #{total_count} clients"
    list.each_with_index do |item, i|
      STDOUT.print "#{i + 1}...#{item.inspect}...."
        _id = item["iD"].try(:strip)
      _params = item.delete_if {|k,v| k.match(/(id|status|date)/i) }
      _params.merge!(bm_id: _id)
      STDOUT.puts "...Id..#{_id}...#{_params.inspect}"
      c = Client.new(_params)
      if c.save
        STDOUT.puts "Saved"
      else
        STDOUT.puts "Errors: #{c.errors.full_messages.inspect}"
      end
    end
  end
      
end
