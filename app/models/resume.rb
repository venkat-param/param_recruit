class Resume
  include Mongoid::Document
	include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  include Mongoid::Search

  field :bm_id, type: String
  field :applicantID, type: String
  field :createDate, type: Time
  field :content, type: String
  field :applicant_id, type: BSON::ObjectId

  # belongs_to :applicant, class_name: "Applicant", foreign_key: "applicant_id"
  validates :bm_id, uniqueness: true

  search_in :content

#  after_create :set_applicant_data
#
#  def set_applicant_data
#    params = CrawlResume.parse(self.content)
#    applicant = Applicant.where(email: params["email"]).first
#    if applicant.present?
#      self.update(:applicant_id => applicant.id)
#    else
#      applicant = Applicant.new(params)
#      if applicant.save(validate: false)
#        self.update(:applicant_id => applicant.id)
#      end
#    end
#    false
#  end


  def self.import_bmdata_from_xml
    obj1 = Parsexml.new
    total_count, list = obj1.get_files("public", "resume")
    STDOUT.puts "Processing #{total_count} resumes"
    list.each_with_index do |item, i|
      _id = item["iD"].try(:strip)
      applicantId = item["applicantID"].try(:strip)
      user = Applicant.where(bm_id: applicantId).first
      STDOUT.print "#{i + 1}...#{applicantId}.......#{item.keys.inspect}..."
      _params = item.delete_if {|k,v| k.match(/(iD|name)/) }
      _params.merge!(bm_id: _id)
      _params.merge!(applicant_id: user.id) if user.present?
      Rails.logger.debug "...333...#{_params.keys.inspect}"
      c = Resume.new(_params)
      if c.save
        #STDOUT.print "Saved"
      else
        STDOUT.puts "Errors: .#{_id}..#{c.errors.full_messages.inspect}"
      end
    end
  end

end
