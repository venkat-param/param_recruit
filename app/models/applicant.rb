class Applicant
  include Mongoid::Document
	include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  include Mongoid::Search


  field :bm_id, type: String
  field :username, type: String
  field :firstName, type: String
  field :lastName, type: String
  field :email, type: String
  field :phone, type: String
  field :city, type: String
  field :region, type: String
  field :postalCode, type: String
  field :country, type: String
  field :currentPay, type: String
  field :currentayType, type: String
  field :taxID, type: String
  field :veteranStatus, type: String
  field :disabledStatus, type: String
  field :employmentType, type: String
  field :race, type: String
  field :status, type: String
  field :source, type: String
  field :gender, type: String
  field :visaStatus, type: String
  field :createUserID, type: String
  field :salutation, type: String
  field :workEmail, type: String
  field :primarySkills, type: String

  validates :firstName, presence: true
  # validates :firstName, uniqueness: true
  validates :bm_id, uniqueness: true

  has_one :resume, dependent: :destroy

  search_in :userName, :firstName, :email, :workEmail, :primarySkills, :lastName,
  :city, :region, :postalCode, :veteranStatus, :VisaStatus #, tags: :name, category: :name, info: %i[summary description]


  def self.import_bmdata_from_xml
    obj1 = Parsexml.new
    total_count, list = obj1.get_files("public", "applicants")
    STDOUT.puts "Processing #{total_count} applicants"
    list.each_with_index do |item, i|
      _id = item["iD"].try(:strip)
      STDOUT.print "#{i + 1}......."
      _params = item.delete_if {|k,v| k.match(/(iD|officeID|clientID|date|modifyUserID|employeeID)/i) }
      _params.merge!(bm_id: _id)
      #STDOUT.puts "......#{_params.inspect}"
      c = Applicant.new(_params)
      if c.save
        #STDOUT.print "Saved"
      else
        STDOUT.puts "Errors: .#{_id}..#{c.errors.full_messages.inspect}"
      end
    end
  end

  def resume_present?
    res = Resume.where(applicantID: bm_id).first
    res.present?
  end
end
