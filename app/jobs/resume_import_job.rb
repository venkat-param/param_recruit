class ResumeImportJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # Do something later
    Resume.import_bmdata_from_xml
  end
end

# ResumeImportJob.perform_later
# rails runner -e production "ResumeImportJob.perform_later"
