class ResumeParserJob < ApplicationJob
  queue_as :resume_parser_job

  def perform(*args)
    # Do something later
    #Resume.import_bmdata_from_xml
    resumes = Resume.where({:bm_id => nil, :applicant_id => nil})
    resumes.each do |resume|
      params = CrawlResume.parse(resume.content)
      applicant = Applicant.where(email: params["email"]).first
      if applicant.present?
        resume.update(:applicant_id => applicant.id)
      else
        applicant = Applicant.new(params)
        if applicant.save(validate: false)
          resume.update(:applicant_id => applicant.id)
        end  
      end
    end
  end
end

# ResumeParserJob.perform_later
# rails runner "ResumeParserJob.perform_later"
