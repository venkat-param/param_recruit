class ApplicantImportJob < ApplicationJob
  queue_as :applicant_import_job

  def perform(*args)
    # Do something later
    Applicant.import_bmdata_from_xml
  end
end

# ApplicantImportJob.perform_now
# rails runner -e production "ApplicantImportJob.perform_now"
