// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require ckeditor-jquery
//= require turbolinks
//= require twitter/bootstrap
//= require_tree .

$(document).ready(function(e){

  $('.search-panel .dropdown-menu').find('a').click(function(e) {
    e.preventDefault();
    var param = $(this).attr("href").replace("#","");
    var concept = $(this).text();
    $('.search-panel span#search_concept').text(concept);
    $('.input-group #search_param').val(param);
  });
});

$(function() {
  return $('[data-behavior~=ajax-spin').on('click', function() {
    var target;
    target = $(this).data('target');
    return $(target).spin('large');
  });
});


$(function(){
  /* Your JavaScript goes here... */
      $('[data-toggle="tooltip"]').tooltip()
});
