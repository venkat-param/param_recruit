class ResumesController < ApplicationController
  before_action :load_resume, only: [:edit, :update, :show, :destroy]

  def index
    @resumes = Resume.all
  end

  def search
    if params[:term].present?
      str = params[:match].presence || "all"
      @resumes = Resume.full_text_search("#{params[:term]}", match: str.to_sym).paginate(page: params[:page], per_page: 100)
       #.where(:email => /#{params[:term]}/i)
       respond_to do |format|
          format.js
          format.html
        end
    else
      @resumes = Resume.none
    end
  end

  def new
    @resume = Resume.new
  end

  def create
    @resume = Resume.new(resume_params)
    if @resume.save(validate: false)
      redirect_to resume_path(@resume)
    else
      Rails.logger.debug "\n\n...ERR..#{@resume.errors.inspect}...\n"
      redirect_to search_applicants_path
      #render action: :new
    end
  end

  def edit
    @resume = Resume.find(params[:id])
  end

  def update
    if @resume.update(resume_params)
      redirect_to resumes_path
    else
      render action: :edit
    end
  end

  def show
    @details = CrawlResume.parse(@resume.content)
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "show"   # Excluding ".pdf" extension.
      end
    end
  end

  def destroy
    @resume.destroy
    redirect_to resumes_path
  end

  private
  def load_resume
    @resume = Resume.find(params[:id])
  end

  def resume_params
    params.permit(:content, :applicant_id)
  end

end
