class ApplicantsController < ApplicationController
  before_action :load_applicant, except: [:index, :new, :create, :search, :search_applicant, :resume]

  def index
    @applicants = Applicant.only(:bm_id, :firstName, :lastName, :email, :primarySkills).paginate(page: params[:page], per_page: 100)
  end

  def search
    if params[:term].present?
      str = params[:match].presence || "all"
      @applicants = Applicant.full_text_search("#{params[:term]}", match: str.to_sym).paginate(page: params[:page], per_page: 100)
       #.where(:email => /#{params[:term]}/i)
       respond_to do |format|
          format.js
          format.html
        end
    else
      @applicants = Applicant.none
    end
  end

  def new
    @applicant = Applicant.new
  end

  def create
    @applicant = Applicant.new(applicant_params)
    if @applicant.save
      redirect_to applicants_path
    else
      Rails.logger.debug "\n\n..ERROR...#{@applicant.errors.full_messages.inspect}....\n"
      redirect_to resume_path(params[:resume_id]), alert: "#{@applicant.errors.full_messages}"
    end
  end

  def edit
    @applicant = Applicant.find(params[:id])
  end

  def update
    if @applicant.update(applicant_params)
      redirect_to applicants_path
    else
      render action: :edit
    end
  end

  def show
  end

  def destroy
    @applicant.destroy
    redirect_to applicants_path
  end

  def view_resume
    Rails.logger.debug "\n\n....app...#{@applicant.inspect}......\n\n"
    res = Resume.where(applicant_id: @applicant.id).first
      Rails.logger.debug "\n\n....es...#{res.inspect}......\n\n"
    @resume = @applicant.resume
    Rails.logger.debug "\n\n....res...#{@resume.inspect}......\n\n"
  end

  def resume
    @resume = Resume.where(applicantID: params[:id]).first
    Rails.logger.debug "\n\n....222...#{@resume.inspect}......\n\n"
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "resume"   # Excluding ".pdf" extension.
      end
    end
  end

  private
  def load_applicant
    @applicant = Applicant.find(params[:id])
  end

  def applicant_params
    params.permit(:username, :firstName, :email, :bm_id, :phone, :state, :postalCode, :primarySkills)
  end

end
