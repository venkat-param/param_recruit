class ClientsController < ApplicationController
  before_action :load_client, only: [:edit, :update, :show, :destroy]
  
  def index
    @clients = Client.all
  end

  def new
    @client = Client.new
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      redirect_to clients_path
    else
      render action: :new
    end
  end

  def edit
    @client = Client.find(params[:id])
  end

  def update
    if @client.update(client_params)
      redirect_to clients_path
    else
      render action: :edit
    end
  end

  def show
  end

  def destroy
    @client.destroy
    redirect_to clients_path
  end
  
  private
  def load_client
    @client = Client.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:name, :website, :address1, :address2, :municipality, :postalcode, 
:discountpct, :discountfee, :stockstymbol, :info)
  end

end
