require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  resources :clients
  resources :applicants do
    member do
      get 'view_resume'
      get 'resume'
    end
    collection do
      get "search"
    end
  end
  resources :resumes do
    collection do
      get "search"
    end
  end

  root to: "clients#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
