Rails version: 5.1.4
Ruby version: 2.3.1 (x86_64-linux)

#TODO:
Ref:
http://blog.scoutapp.com/articles/2016/02/16/which-ruby-background-job-framework-is-right-for-you
https://ryanboland.com/blog/writing-your-first-background-worker/
http://guides.rubyonrails.org/active_job_basics.html
http://tutorials.jumpstartlab.com/topics/performance/background_jobs.html
https://blog.codeship.com/how-to-use-rails-active-job/

# Sidekiq queue patterns
http://erik.debill.org/2016/08/07/sidekiq-queueing-patterns

# to run sidekiq
sidekiq -C config/sidekiq.yml
bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml -e production

https://stackoverflow.com/questions/16835963/sidekiq-not-processing-queue

https://stackoverflow.com/questions/22958188/how-to-run-sidekiq-in-production-server

https://bearmetal.eu/theden/how-do-i-know-whether-my-rails-app-is-thread-safe-or-not/

#redis server
# to check redis server is running or not
redis-cli ping
# PONG

# to check redis server is installed or not
redis-server --version

# if we get redis error says "Error connecting to Redis on 127.0.0.1:6379 (Errno::ECONNREFUSED)"

sudo apt-get install redis-tools

sudo apt-get install redis-server

redis-server &

#ckeditor
https://github.com/tsechingho/ckeditor-rails

# resume display as a pdf
http://thelazylog.com/how-to-convert-html-views-into-pdf-in-rails/

#spinners
http://tobiasahlin.com/spinkit/

# MongoDB doesn't support joins
https://stackoverflow.com/questions/20565222/join-2-tables-in-mongodb-with-rails

## multi-tenenant rails application
https://gorails.com/episodes/multitenancy-with-apartment
