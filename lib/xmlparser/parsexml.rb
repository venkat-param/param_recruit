require 'nokogiri'
require 'benchmark'
require_relative './base'
require_relative './dir_search'
require_relative './parse'

class Parsexml < Base
  
  def get_files(dir, filename)
    obj = DirSearch.new(dir, filename)
    @files = []
    searchtime = Benchmark.measure {
      @files = obj.printio
    }  
    p = Parse.new
    parsertime = Benchmark.measure {
      @files.each do |path|
        p.parser(path)
      end
    }  
    count, list = p.total
#    ps "total items count: #{count}"
#    ps "list is: #{list}"
    ps "SearchTime is: #{searchtime}", "brown"
    ps "ParserTime is: #{parsertime}", "yellow"
    [count, list]
  end
  
end

#obj1 = Parsexml.new
#obj1.get_files
