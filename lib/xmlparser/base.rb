require_relative './colored_text'

class Base

  def ps(text, tcolor="red", bgcolor="default")
    STDOUT.puts ColoredText.colorize(text, tcolor, bgcolor)
  end

end
