require 'nokogiri'
require_relative './base'

#TODO: BasePath is working on Ubuntu & Mac, need to test it on windows
# if it is not working then use has to enter the directory path inaddition to the directory name
class DirSearch < Base

  def initialize(dir, filename)
    @basePath = Dir.pwd
    @baseDir = dir
    @file = filename
## commented because running the data import from Background Job  
#    @currentDir = File.basename(Dir.getwd)
#    @basePath = File.expand_path("..", Dir.pwd) #TODO: check in windows
#    ps "Do you want to search in current directory?(y/n)"
#    choice = STDIN.gets.chomp.downcase
#    case choice[0]
#      when "y"
#        @baseDir = @currentDir
#      when "n"
#        ps "Please enter directory path where you want to search files"
#        @baseDir = STDIN.gets.chomp
#        @basePath = Dir.pwd
#    end
#    ps "Enter keyword from the filename?"
#    @file = STDIN.gets.chomp
  end
  
  def printio
    STDOUT.puts "#{@basePath}/#{@baseDir}/**/#{@file}*.xml"
    STDOUT.print "Searching for the files...."
    files = Dir.glob("#{@basePath}/#{@baseDir}/**/#{@file}*.xml")
    ps " completed: #{files.count} files found.", "green"
    files
  end
end

#obj = DirSearch.new
#obj.printio


