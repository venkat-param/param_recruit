require 'nokogiri'
require_relative './base'

class Parse < Base

  def initialize
    @@total = {}
    @@list = []
#    ps "Enter the content tag of the item(Eg: Content, Email):"
#    @contentTag = gets.chomp
  end
  
  def parser(path)
    STDOUT.print "Parsing....#{path} : "
    ## read xml file content 
    f = File.open(path)
    doc = Nokogiri::XML(f)
    f.close

    ## Fetch resume nodes from xml
#    blocks = doc.xpath("//#{@listTag}/#{@itemTag}/#{@contentTag}")
     blocks = doc.xpath("//#{doc.root.name[0...-1]}")
  
    ## Print resumes count present in the xml file
    #ps blocks.count
    @@total[path] = blocks.count
    blocks.each_with_index do |item, i|
     nodes = item.xpath('./*')
     h = {}
     nodes.each {|n| k = n.name; h[k[0].downcase + k[1..-1]] = n.text; }
     @@list.push(h)
    end
    ps blocks.count
  end
    
  def total
    #ps "\n........Total Records............\n"
    #ps "...File Name.........Count.."
    grandtotal = 0
    @@total.each do |k, v|
      #ps "...#{k}.........#{v}.."
      grandtotal += v
    end
    ps "\n........Total Records are....#{grandtotal}........\n", "green"
    [grandtotal, @@list]
  end

end


